# Frama subtheme

Un sous-thème Drupal « Framasoft » pour Yakforms.

Pour l'installer :
```bash
cd sites/all/themes
git clone [url de ce répo]
drush en yaktheme_frama
```

Puis se rendre sur `yoursite.tdl/admin/config/user-interface/themekey/settings/ui` et dans la section `Selectable themes` cocher la case à côté du thème, et `Enregistrer`.

Pour rendre ce thème par défaut : `yoursite.tdl/admin/appearance`, et configurer ce thème comme thème par défaut.
